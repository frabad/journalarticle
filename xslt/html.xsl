﻿<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
<!ENTITY css SYSTEM "html.styles.css">
]>
<xsl:transform version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:db="http://docbook.org/ns/docbook"
  xmlns="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="svg db html">

  <xsl:output
    method="xml" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
    doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"/>

  <xsl:strip-space elements="*"/>

  <xsl:variable name="css">&css;</xsl:variable>
  <xsl:variable name="img.width.default">192</xsl:variable>
  <xsl:param name="title.link.source" select="false()"/>
  <xsl:param name="w3c.validator.badge" select="true()"/>

  <xsl:template match="*">
    <div class="{local-name()}">
      <xsl:comment>
        <xsl:value-of select="concat('/!\ no match for ', local-name())"/>
      </xsl:comment>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template
    match="db:author |
    db:pubdate |
    db:bibliosource |
    db:issn |
    db:editor |
    db:publishername |
    db:copyright |
    db:personname |
    db:subjectset |
    db:caption |
    db:section |
    db:simplesect |
    db:sidebar">
    <div>
      <xsl:attribute name="class">
        <xsl:value-of select="local-name()"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="db:article">
    <html>
      <xsl:call-template name="html-head"/>
      <body>
        <xsl:attribute name="class">article</xsl:attribute>
        <xsl:apply-templates/>
        <xsl:if test="$w3c.validator.badge">
          <xsl:call-template name="w3c.validator.badge"/>
        </xsl:if>
      </body>
    </html>
  </xsl:template>

  <xsl:template name="html-head">
    <head>
      <title>
        <xsl:value-of select="normalize-space(db:info/db:title)"/>
      </title>

      <!-- FIXME: manquent toutes les méta-infos -->

      <style>
        <xsl:attribute name="type">text/css</xsl:attribute>
        <xsl:call-template name="css-stylesheet"/>
      </style>
    </head>
  </xsl:template>

  <xsl:template match="db:article/db:info">
    <div>
      <xsl:attribute name="class">
        <xsl:value-of select="local-name()"/>
      </xsl:attribute>
      <xsl:apply-templates select="db:subjectset"/>
      <xsl:apply-templates select="db:title">
        <xsl:with-param name="source" select="db:bibliosource"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="db:mediaobject[1]"/>
      <xsl:apply-templates select="db:abstract"/>
      <xsl:apply-templates select="db:authorgroup"/>
      <xsl:apply-templates select="db:pubdate"/>

      <!--FIXME ajouter ici et dans l'ordre les objets
      à afficher dans l'en-tête -->

    </div>
  </xsl:template>

  <xsl:template match="db:section/db:info | db:simplesect/db:info | db:sidebar/db:info">
    <div>
      <xsl:attribute name="class">
        <xsl:value-of select="local-name()"/>
      </xsl:attribute>
      <xsl:apply-templates select="db:title">
        <xsl:with-param name="source" select="db:bibliosource"/>
      </xsl:apply-templates>
      <xsl:apply-templates select="db:pubdate"/>
      <xsl:apply-templates select="db:abstract"/>

      <!--FIXME ajouter ici et dans l'ordre les objets
      à afficher dans l'en-tête de la section -->

    </div>
  </xsl:template>

  <xsl:template match="db:title">
    <xsl:param name="source"/>
    <xsl:param name="contenu">
      <xsl:choose>
        <xsl:when test="$source != '' and $title.link.source = true()">
          <a>
            <xsl:attribute name="href">
              <xsl:value-of select="$source"/>
            </xsl:attribute>
            <xsl:apply-templates/>
          </a>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    <xsl:choose>
      <xsl:when test="parent::db:info/parent::db:article">
        <h1>
          <xsl:attribute name="class">
            <xsl:value-of select="local-name()"/>
          </xsl:attribute>
          <xsl:copy-of select="$contenu"/>
        </h1>
      </xsl:when>
      <xsl:when
        test="parent::db:info/parent::db:section or
        parent::db:info/parent::db:simplesect">
        <h2>
          <xsl:attribute name="class">
            <xsl:value-of select="local-name()"/>
          </xsl:attribute>
          <xsl:copy-of select="$contenu"/>
        </h2>
      </xsl:when>
      <xsl:when test="parent::db:info/parent::db:sidebar">
        <h3>
          <xsl:attribute name="class">
            <xsl:value-of select="local-name()"/>
          </xsl:attribute>
          <xsl:copy-of select="$contenu"/>
        </h3>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="db:pubdate">
    <xsl:param name="label">Publié le </xsl:param>
    <div>
      <xsl:attribute name="class">date</xsl:attribute>
      <xsl:value-of select="$label"/>
      <xsl:apply-templates/>
    </div>
  </xsl:template>

  <xsl:template match="db:article/db:info/db:abstract">
    <xsl:element name="div">
      <xsl:attribute name="id">
        <xsl:value-of select="name()"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="db:mediaobject/db:info/db:abstract">
    <!-- normalement identique à db:caption-->
  </xsl:template>

  <xsl:template match="db:simpara">
    <p>
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="db:mediaobject | db:imageobject ">
    <xsl:param name="width.attribute"
               select="normalize-space(descendant::db:imagedata/@width)"/>
    <xsl:param name="width">
      <xsl:choose>
        <xsl:when test="$width.attribute != ''">
          <xsl:value-of select="$width.attribute"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$img.width.default"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    <div>
      <xsl:attribute name="class">
        <xsl:value-of select="local-name()"/>
      </xsl:attribute>
      <xsl:attribute name="style">
        <xsl:value-of select="concat('max-width : ',$width,'px')"/>
      </xsl:attribute>
      <xsl:apply-templates>
        <xsl:with-param name="width" select="$width"/>
      </xsl:apply-templates>
    </div>
  </xsl:template>

  <xsl:template match="db:imagedata">
    <xsl:param name="width"/>
    <xsl:param name="depth.attribute" select="normalize-space(@depth)"/>
    <xsl:param name="height">
      <xsl:choose>
        <xsl:when test="$depth.attribute != ''">
          <xsl:value-of select="$depth.attribute"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="3 * $width div 4"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    <img>
      <xsl:attribute name="src">
        <xsl:value-of select="attribute::fileref"/>
      </xsl:attribute>
      <xsl:attribute name="alt">
        <xsl:value-of select="attribute::fileref"/>
      </xsl:attribute>
      <xsl:attribute name="width">
        <xsl:value-of select="concat($width,'px')"/>
      </xsl:attribute>
      <xsl:attribute name="height">
        <xsl:value-of select="concat($height,'px')"/>
      </xsl:attribute>
    </img>
  </xsl:template>

  <xsl:template match="db:remark">
  </xsl:template>
  
  <xsl:template match="db:emphasis">
    <em>
      <xsl:apply-templates/>
    </em>
  </xsl:template>

  <xsl:template match="db:abbrev">
    <!-- FIXME: comprendre le mécanisme de définitions d'abbréviations de docbook-->
    <abbr>
      <xsl:attribute name="title">
        <xsl:apply-templates select="text()"/>
      </xsl:attribute>
      <xsl:attribute name="onmouseover">
        <xsl:text>javascript:this.style.cursor='help';</xsl:text>
      </xsl:attribute>
      <xsl:value-of select="normalize-space(db:alt)"/>
    </abbr>
  </xsl:template>

  <xsl:template match="db:quote">
    <xsl:param name="contenu">
      <xsl:apply-templates/>
    </xsl:param>
    <xsl:param name="q.open">
      <xsl:choose>
        <xsl:when test="lang('fr')">«&#x202F;</xsl:when>
        <xsl:when test="lang('it')">«&#x202F;</xsl:when>
        <xsl:otherwise>&#8220;</xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    <xsl:param name="q.close">
      <xsl:choose>
        <xsl:when test="lang('fr')">&#x202F;»</xsl:when>
        <xsl:when test="lang('it')">&#x202F;»</xsl:when>
        <xsl:otherwise>&#8221;</xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    
    <em><xsl:value-of select="concat($q.open,$contenu,$q.close)"/></em>
  </xsl:template>

  <xsl:template match="db:note">
    <span>
      <xsl:attribute name="class">
        <xsl:value-of select="local-name()"/>
      </xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="normalize-space()"/>
      </xsl:attribute>
      <xsl:attribute name="longdesc">
        <xsl:value-of select="normalize-space()"/>
      </xsl:attribute>
      <xsl:attribute name="onmouseover">
        <xsl:text>javascript:this.style.cursor='help';</xsl:text>
      </xsl:attribute>

      <xsl:text>*</xsl:text>

    </span>
  </xsl:template>

  <xsl:template match="db:link">
    <a>
      <xsl:attribute name="href">
        <xsl:value-of select="@xlink:href"/>
      </xsl:attribute>
      <xsl:attribute name="title">
        <xsl:value-of select="@xlink:href"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </a>
  </xsl:template>

  <xsl:template match="db:subject">
    <span>
      <xsl:attribute name="class">
        <xsl:value-of select="local-name()"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </span>
    <xsl:if test="following-sibling::db:subject">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="db:subjectterm">
    <xsl:value-of select="normalize-space()"/>
  </xsl:template>

  <xsl:template name="css-stylesheet">
    <xsl:value-of select="normalize-space($css)"/>
  </xsl:template>

  <xsl:template name="w3c.validator.badge">
    <p class="badge"><a href="https://validator.w3.org/#validate_by_input"><img
      src="http://www.w3.org/Icons/valid-xhtml10"
      alt="Valid XHTML 1.0 Strict" height="31" width="88"
      /></a></p>
  </xsl:template>

</xsl:transform>

