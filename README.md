# Journalarticle-xml

modèle XML d'article de presse inspiré de Docbook

Des feuilles de transformation (XSL-T) et une interface en ligne de commande
(Python) permettent de produire des documents dans différents formats de
publication (HTML, XSL-FO / PDF). Invoquez le script `artconv.py` fourni pour
afficher son utilisation.

