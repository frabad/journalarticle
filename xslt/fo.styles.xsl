<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
  xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:db="http://docbook.org/ns/docbook">


  <!-- == styles == -->

  <xsl:attribute-set name="sty.d�faut">
    <xsl:attribute name="font-family">Libertinus, serif</xsl:attribute>
    <xsl:attribute name="font-size">10pt</xsl:attribute>
    <xsl:attribute name="text-align">justify</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.t�te-page">
    <xsl:attribute name="padding-after">1mm</xsl:attribute>
    <xsl:attribute name="padding-before">1mm</xsl:attribute>
    <xsl:attribute name="border-color">#000000</xsl:attribute>
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-width">1px</xsl:attribute>
    <xsl:attribute name="font-size">0.8em</xsl:attribute>
    <xsl:attribute name="text-align">end</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.pied-page">
    <xsl:attribute name="padding-after">1mm</xsl:attribute>
    <xsl:attribute name="padding-before">1mm</xsl:attribute>
    <xsl:attribute name="border-color">#000000</xsl:attribute>
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="border-width">1px</xsl:attribute>
    <xsl:attribute name="font-size">0.8em</xsl:attribute>
    <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.arch-page">
    <xsl:attribute name="margin-top">10mm</xsl:attribute>
    <xsl:attribute name="margin-bottom">10mm</xsl:attribute>
    <xsl:attribute name="margin-left">20mm</xsl:attribute>
    <xsl:attribute name="margin-right">20mm</xsl:attribute>
    <xsl:attribute name="page-height">297mm</xsl:attribute>
    <xsl:attribute name="page-width">210mm</xsl:attribute>
    <!-- FIXME: faire un sty.marges -->
    <!-- FIXME: pr�voir plusieurs formats de page -->
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.m�tabloc">
    <xsl:attribute name="border-color">#BBBBBB</xsl:attribute>
    <xsl:attribute name="border-before-style">dotted</xsl:attribute>
    <xsl:attribute name="border-width">1px</xsl:attribute>
    <xsl:attribute name="color">#0A0A0A</xsl:attribute>
    <xsl:attribute name="font-size">0.7em</xsl:attribute>
    <xsl:attribute name="font-style">normal</xsl:attribute>
    <xsl:attribute name="padding-before">0.4em</xsl:attribute>
    <xsl:attribute name="space-before">1em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="text-align-last">start</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.r�sum�-article">
    <xsl:attribute name="font-family">Libertinus Sans, sans</xsl:attribute>
    <xsl:attribute name="font-style">normal</xsl:attribute>
    <xsl:attribute name="font-size">1.0em</xsl:attribute>
    <xsl:attribute name="hyphenate">false</xsl:attribute>
    <xsl:attribute name="text-align">justify</xsl:attribute>
    <xsl:attribute name="text-align-last">start</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.t�te-article">
    <xsl:attribute name="border-width">1px</xsl:attribute>
    <xsl:attribute name="border-color">#BBBBBB</xsl:attribute>
    <xsl:attribute name="border-before-style">none</xsl:attribute>
    <xsl:attribute name="border-after-style">dotted</xsl:attribute>
    <xsl:attribute name="padding-after">0mm</xsl:attribute>
    <xsl:attribute name="space-after">4mm</xsl:attribute>
    <xsl:attribute name="space-after.conditionality">retain</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.signature">
    <xsl:attribute name="keep-together">always</xsl:attribute>
    <xsl:attribute name="keep-with-previous">always</xsl:attribute>
    <xsl:attribute name="space-before">1em</xsl:attribute>
    <xsl:attribute name="text-align">end</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.corps-article"> </xsl:attribute-set>

  <xsl:attribute-set name="sty.paragraphe">
    <xsl:attribute name="text-align">justify</xsl:attribute>
    <xsl:attribute name="text-indent">
      <xsl:choose>
        <xsl:when test="preceding-sibling::*[1][self::db:info]">0em</xsl:when>
        <xsl:otherwise>2em</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="space-before.minimum">0em</xsl:attribute>
    <xsl:attribute name="space-before.optimum">0.0em</xsl:attribute>
    <xsl:attribute name="space-before.maximum">0.4em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.illustration">
    <xsl:attribute name="keep-together.within-column">always</xsl:attribute>
    <xsl:attribute name="space-before.optimum">1mm</xsl:attribute>
    <xsl:attribute name="space-after.optimum">1mm</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.image">
    <xsl:attribute name="text-align">center</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.description-image">
    <xsl:attribute name="color">#0C0C0C</xsl:attribute>
    <xsl:attribute name="font-size">0.8em</xsl:attribute>
    <xsl:attribute name="font-style">italic</xsl:attribute>
    <xsl:attribute name="space-before.maximum">0.4em</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="text-align-last">start</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.lien">
    <xsl:attribute name="color">blue</xsl:attribute>
    <xsl:attribute name="hyphenate">true</xsl:attribute>
    <xsl:attribute name="hyphenation-character">-</xsl:attribute>
    <xsl:attribute name="wrap-option">wrap</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.emphase">
    <xsl:attribute name="font-style">italic</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.citation">
    <xsl:attribute name="font-style">normal</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.citation.bloc">
    <xsl:attribute name="border-left-style">solid</xsl:attribute>
    <xsl:attribute name="border-width">thick</xsl:attribute>
    <xsl:attribute name="border-color">gray</xsl:attribute>
    <xsl:attribute name="font-style">italic</xsl:attribute>
    <xsl:attribute name="space-after.maximum">0.6em</xsl:attribute>
    <xsl:attribute name="space-after.minimum">0.2em</xsl:attribute>
    <xsl:attribute name="space-before.maximum">0.5em</xsl:attribute>
    <xsl:attribute name="padding-start">1em</xsl:attribute>
    <xsl:attribute name="start-indent">1.2em</xsl:attribute>
    <xsl:attribute name="text-align">justify</xsl:attribute>
    <xsl:attribute name="text-align-last">start</xsl:attribute>
    <xsl:attribute name="text-indent">0em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.citation.attribution">
    <xsl:attribute name="font-size">0.9em</xsl:attribute>
    <xsl:attribute name="font-style">normal</xsl:attribute>
    <xsl:attribute name="keep-with-previous">always</xsl:attribute>
    <xsl:attribute name="space-before.minimum">0.2em</xsl:attribute>
    <xsl:attribute name="text-align">end</xsl:attribute>
    <xsl:attribute name="text-align-last">end</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.code">
    <xsl:attribute name="font-family">Libertinus Mono, monospace</xsl:attribute>
    <xsl:attribute name="font-size">0.9em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.uri">
    <xsl:attribute name="font-family">Libertinus Mono, monospace</xsl:attribute>
    <xsl:attribute name="font-size">0.9em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.code.bloc" use-attribute-sets="sty.code">
    <xsl:attribute name="font-size">0.7em</xsl:attribute>
    <xsl:attribute name="space-before">0.7em</xsl:attribute>
    <xsl:attribute name="space-after">1em</xsl:attribute>
    <xsl:attribute name="border-width">1px</xsl:attribute>
    <xsl:attribute name="border-color">silver</xsl:attribute>
    <xsl:attribute name="border-before-style">dotted</xsl:attribute>
    <xsl:attribute name="border-after-style">dotted</xsl:attribute>
    <xsl:attribute name="padding-top">0.7em</xsl:attribute>
    <xsl:attribute name="padding-bottom">0.7em</xsl:attribute>
    <xsl:attribute name="color">#0D0D0D</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="white-space-treatment">preserve</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
    <xsl:attribute name="white-space-collapse">false</xsl:attribute>
    <xsl:attribute name="text-align-last">start</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.ui">
    <xsl:attribute name="font-family">Libertinus Sans, sans</xsl:attribute>
    <xsl:attribute name="font-size">0.9em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.abbrev">
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.titre">
    <!-- styles de titre communs -->
    <xsl:attribute name="font-family">Libertinus Sans, sans</xsl:attribute>
    <xsl:attribute name="hyphenate">false</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="text-align-last">start</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.titre-article" use-attribute-sets="sty.titre">
    <xsl:attribute name="color">#000000</xsl:attribute>
    <xsl:attribute name="font-size">2.2em</xsl:attribute>
    <xsl:attribute name="space-after.minimum">2mm</xsl:attribute>
    <xsl:attribute name="space-after.optimum">4mm</xsl:attribute>
    <xsl:attribute name="space-after.maximum">1em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.titre-section" use-attribute-sets="sty.titre">
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="font-size">1.2em</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute>
    <xsl:attribute name="padding-bottom">2pt</xsl:attribute>
    <xsl:attribute name="space-after">4pt</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.section">
    <xsl:attribute name="space-before.minimum">0.6em</xsl:attribute>
    <xsl:attribute name="space-before.optimum">1em</xsl:attribute>
    <xsl:attribute name="space-before.maximum">1.2em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.titre-encadr�" use-attribute-sets="sty.titre">
    <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
    <xsl:attribute name="border-bottom-width">1pt</xsl:attribute>
    <xsl:attribute name="border-bottom-color">white</xsl:attribute>
    <xsl:attribute name="font-size">1em</xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute>
    <xsl:attribute name="padding-after">0.4em</xsl:attribute>
    <xsl:attribute name="space-after">0.6em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.encadr�" use-attribute-sets="sty.section">
    <xsl:attribute name="background-color">#DADADA</xsl:attribute>
    <xsl:attribute name="end-indent">0.6em</xsl:attribute>
    <xsl:attribute name="font-size">0.9em</xsl:attribute>
    <xsl:attribute name="keep-together.within-column">2</xsl:attribute>
    <xsl:attribute name="padding">0.6em</xsl:attribute>
    <xsl:attribute name="start-indent">0.6em</xsl:attribute>
    <xsl:attribute name="space-after">0.6em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.bloc-notes">
    <xsl:attribute name="border-top-style">solid</xsl:attribute>
    <xsl:attribute name="border-top-width">thin</xsl:attribute>
    <xsl:attribute name="color">black</xsl:attribute>
    <xsl:attribute name="font-size">0.9em</xsl:attribute>
    <xsl:attribute name="padding-top">0.6em</xsl:attribute>
    <xsl:attribute name="space-before">2em</xsl:attribute>
    <xsl:attribute name="provisional-distance-between-starts">10mm</xsl:attribute>
    <xsl:attribute name="provisional-label-separation">2mm</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.numnote">
    <xsl:attribute name="baseline-shift">0.3em</xsl:attribute>
    <xsl:attribute name="font-size">0.7em</xsl:attribute>
    <xsl:attribute name="color">black</xsl:attribute>
    <xsl:attribute name="background-color">#DADADA</xsl:attribute>
    <xsl:attribute name="keep-with-previous.within-line">always</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.note">
    <xsl:attribute name="space-before">0.3em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.note.label">
    <xsl:attribute name="background-color">#DADADA</xsl:attribute>
    <xsl:attribute name="text-align">center</xsl:attribute>
    <xsl:attribute name="text-align-last">center</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.note.corps">
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="text-align-last">start</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.liste">
    <xsl:attribute name="provisional-distance-between-starts">2em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.liste.item">
    <xsl:attribute name="space-before">4pt</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.liste.item.label">
    <xsl:attribute name="start-indent">0.6em</xsl:attribute>
    <xsl:attribute name="text-align">end</xsl:attribute>
    <xsl:attribute name="text-align-last">end</xsl:attribute>
    <xsl:attribute name="end-indent">label-end()</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.liste.item.corps">
    <xsl:attribute name="start-indent">body-start()</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="sty.m�tadonn�e"> </xsl:attribute-set>

  <!-- styles d'�l�ments d'apr�s leur nom -->

  <xsl:attribute-set name="url" use-attribute-sets="sty.uri">
  </xsl:attribute-set>
  
  <xsl:attribute-set name="filename" use-attribute-sets="sty.code">
  </xsl:attribute-set>

  <xsl:attribute-set name="code" use-attribute-sets="sty.code">
  </xsl:attribute-set>

  <xsl:attribute-set name="application" use-attribute-sets="sty.emphase">
  </xsl:attribute-set>
  
  <xsl:attribute-set name="link" use-attribute-sets="sty.lien">
  </xsl:attribute-set>

</xsl:transform>
